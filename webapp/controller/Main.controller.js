sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("UI5LifecycleDemo.controller.Main", {
		
		
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf UI5LifecycleDemo.view.Main
		 */
			onInit: function() {
				alert("onInit() of controller called...");
			},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf UI5LifecycleDemo.view.Main
		 */
			onBeforeRendering: function() {
				alert("onBeforeRendering() of controller called...");
			},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf UI5LifecycleDemo.view.Main
		 */
			onAfterRendering: function() {
				alert("onAfterRendering() of controller called...");
			},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf UI5LifecycleDemo.view.Main
		 */
			onExit: function() {
				alert("onExit() of controller called...");
			},

		
		onBtnDestroy: function(){
			// Destroy view and controller
			this.getView().destroy();
		},
		
		onBtnRerender: function(){
			// Rerender view and controller
			this.getView().rerender();
		}
	});
});